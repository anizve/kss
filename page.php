<?php
function rupiah($angka)
{
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
}

function tgl_indo($tanggal)
{
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="Koperasi Kredit Usaha Sejahtera">
    <meta name="author" content="KKUS">

    <title>.:: <?php echo $title;?>  | Koperasi Kredit Usaha Sejahtera ::.</title>
    <link rel="icon" href="favicon.png" sizes="16x16 32x32" type="image/png">
    
    <!-- vendor css -->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">

    <!-- Shamcey CSS -->
    <link rel="stylesheet" href="css/shamcey.css">
    <?php Media('head');?>

  </head>

  <body>

    <div class="sh-logopanel">
      <a href="" class="sh-logo-text">KKUS APP</a>
      <a id="navicon" href="" class="sh-navicon d-none d-xl-block"><i class="icon ion-navicon"></i></a>
      <a id="naviconMobile" href="" class="sh-navicon d-xl-none"><i class="icon ion-navicon"></i></a>
    </div><!-- sh-logopanel -->

    <div class="sh-sideleft-menu">
      <label class="sh-sidebar-label">MENU NAVIGASI</label>
      <ul class="nav">
        <li class="nav-item">
          <a href="?hal=main" class="nav-link<?php if($_GET['hal']=='main'){echo ' active';}?>">
            <i class="icon ion-ios-home-outline"></i>
            <span>Halaman Utama</span>
          </a>
        </li><!-- nav-item -->
        <?php include 'include/menu.php';?>
        
      </ul>
    </div><!-- sh-sideleft-menu -->

    <div class="sh-headpanel">
      <div class="sh-headpanel-left">

      </div><!-- sh-headpanel-left -->

      <div class="sh-headpanel-right">
        
        <div class="dropdown dropdown-notification">
          <a href="" data-toggle="dropdown" class="dropdown-link dropdown-link-notification">
            <i class="icon ion-ios-email tx-24"></i>
            <span class="square-8"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-menu-header">
              <label>Transaksi</label>
            </div><!-- d-flex -->

            <div class="media-list">
              <!-- loop starts here -->
              <a href="" class="media-list-link read">
                <div class="media pd-x-20 pd-y-15">
                  <img src="img/img8.jpg" class="wd-40 rounded-circle" alt="">
                  <div class="media-body">
                    <p class="tx-13 mg-b-0"><strong class="tx-medium">Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                    <span class="tx-12">October 03, 2017 8:45am</span>
                  </div>
                </div><!-- media -->
              </a>
              
              <div class="media-list-footer">
                <a href="" class="tx-12"><i class="fa fa-angle-down mg-r-5"></i> Lihat Semua Transaksi</a>
              </div>
            </div><!-- media-list -->
          </div><!-- dropdown-menu -->
        </div>
        <div class="dropdown dropdown-profile">
          <a href="" data-toggle="dropdown" class="dropdown-link">
            <img src="img/<?php echo $_SESSION['foto'];?>" class="wd-60 rounded-circle" alt="">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="media align-items-center">
              <img src="img/<?php echo $_SESSION['foto'];?>" class="wd-60 ht-60 rounded-circle bd pd-5" alt="">
              <div class="media-body">
                <h6 class="tx-inverse tx-15 mg-b-5"><?php echo $_SESSION['nama'];?></h6>
                <p class="mg-b-0 tx-12"><?php echo $_SESSION['email'];?></p>
              </div><!-- media-body -->
            </div><!-- media -->
            <hr>
            <ul class="dropdown-profile-nav">
              <li><a href="?hal=profile"><i class="icon ion-ios-person"></i> Edit Profile</a></li>
              <li><a href=""><i class="icon ion-ios-gear"></i> Ubah Sandi</a></li>
              <li><a href="?hal=logout"><i class="icon ion-power"></i> Logout</a></li>
            </ul>
          </div><!-- dropdown-menu -->
        </div>
      </div><!-- sh-headpanel-right -->
    </div><!-- sh-headpanel -->

    <div class="sh-mainpanel">
      <div class="sh-breadcrumb">
        <nav class="breadcrumb">
          <?php echo $bc;?>

        </nav>
      </div><!-- sh-breadcrumb -->
      <div class="sh-pagetitle">
        <div class="input-group">
          &nbsp;
        </div><!-- input-group -->
        <div class="sh-pagetitle-left">
          <div class="sh-pagetitle-icon"><i class="icon <?php echo $icon;?>"></i></div>
          <div class="sh-pagetitle-title">
            <span>Koperasi Kredit Usaha Sejahtera</span>
            <h2><?php echo $title;?></h2>
          </div><!-- sh-pagetitle-left-title -->
        </div><!-- sh-pagetitle-left -->
      </div><!-- sh-pagetitle -->

      <div class="sh-pagebody">

      <?php Media('isi');?>

      </div><!-- sh-pagebody -->
      <div class="sh-footer">
        <div>Copyright &copy; 2019. Koperasi Kredit Usaha Sejahtera</div>
        <div class="mg-t-10 mg-md-t-0">Designed by: <a href="?hal=main">KKUS</a></div>
      </div><!-- sh-footer -->
    </div><!-- sh-mainpanel -->

    <script src="lib/jquery/jquery.js"></script>
    <script src="lib/popper.js/popper.js"></script>
    <script src="lib/bootstrap/bootstrap.js"></script>
    <script src="lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>

    <script src="js/shamcey.js"></script>
    <?php Media('foot');?>

  </body>
</html>
