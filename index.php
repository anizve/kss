<?php
session_start();
include 'include/config.php';
if (!isset($_GET['hal'])) 
{
	header('Location: ?hal=main');
}

if (isset($_SESSION['email'])) 
{
	include 'page.php';
}
else
{
	include 'login.php';
}
?>