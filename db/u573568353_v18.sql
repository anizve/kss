-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 20 Bulan Mei 2019 pada 08.47
-- Versi server: 10.2.23-MariaDB
-- Versi PHP: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u573568353_v18`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_anggota`
--

CREATE TABLE `tbl_anggota` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `tmp_lhr` varchar(255) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `tlp` varchar(13) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `mulai_kerja` date NOT NULL,
  `suami_istri` varchar(50) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `saldo_awal` int(13) DEFAULT NULL,
  `saldo_akhir` int(13) DEFAULT NULL,
  `status_anggota` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_anggota`
--

INSERT INTO `tbl_anggota` (`id`, `id_users`, `nik`, `nama_lengkap`, `alamat`, `tmp_lhr`, `tgl_lhr`, `tlp`, `pekerjaan`, `pendidikan`, `mulai_kerja`, `suami_istri`, `tgl_daftar`, `saldo_awal`, `saldo_akhir`, `status_anggota`) VALUES
(1, 11, '32198900803', 'Rudi Hartono', 'kadungora', 'Garut', '2019-05-01', '082134568831', 'buruh', '2', '2019-05-02', 'Nining', '2019-05-06', 500000, 600000, 1),
(2, 12, '987397497927', 'Asri Nur Ajizah', 'Kp. Rengel', 'Bandung', '2019-05-01', '08976543212', 'Ibu Rumah Tangga', '3', '2019-05-02', 'Dadan', '2019-05-04', 200000, 50000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_angsuran`
--

CREATE TABLE `tbl_angsuran` (
  `id` int(11) NOT NULL,
  `id_pinjam` int(11) NOT NULL,
  `id_bayar` int(11) NOT NULL,
  `kode_angsuran` int(11) NOT NULL,
  `nominal_angsuran` int(13) NOT NULL,
  `tgl_angsuran` date NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `status_angsuran` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_angsuran`
--

INSERT INTO `tbl_angsuran` (`id`, `id_pinjam`, `id_bayar`, `kode_angsuran`, `nominal_angsuran`, `tgl_angsuran`, `jatuh_tempo`, `status_angsuran`) VALUES
(1, 12, 23, 1, 300, '2019-05-01', '2019-05-28', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_bayar`
--

CREATE TABLE `tbl_bayar` (
  `id` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `no_rek` int(20) NOT NULL,
  `nominal_trf` int(9) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `tgl_transfer` date NOT NULL,
  `nama_rek` varchar(50) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `cabang_bank` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_bayar`
--

INSERT INTO `tbl_bayar` (`id`, `id_anggota`, `no_rek`, `nominal_trf`, `tgl_bayar`, `tgl_transfer`, `nama_rek`, `nama_bank`, `cabang_bank`, `ket`, `foto`) VALUES
(1, 1, 123, 200000, '2019-05-08', '2019-05-07', 'Dadan Sunarya', 'BNI', 'KAdungora', 'tidak', 'gbr.jpg'),
(2, 1, 1234567890, 50000, '2019-05-08', '2019-05-07', 'Rudi Hartono', 'BNI', 'Kadungora', 'TIdak Ada', 'nota.jpeg'),
(3, 1, 1234567890, 50000, '2019-05-08', '2019-05-07', 'Rudi Hartono', 'BRI', 'Kadungora', 'Tidak ada', 'pramuka.png'),
(4, 1, 1234567890, 50000, '2019-05-08', '2019-05-07', 'Rudi Hartono', 'BRI', 'Kadungora', 'Tidak ada', 'nota.jpeg'),
(5, 2, 1234567890, 500000, '2019-05-08', '2019-05-08', 'Susi Handayani', 'BRI', 'Kadungora', 'Aman', 'Selection_001.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pinjam`
--

CREATE TABLE `tbl_pinjam` (
  `id` int(6) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `nominal_pinjam` int(15) NOT NULL,
  `bunga_pinjam` int(15) NOT NULL,
  `tenor_angsuran` int(4) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pinjam`
--

INSERT INTO `tbl_pinjam` (`id`, `id_anggota`, `tgl_pinjam`, `nominal_pinjam`, `bunga_pinjam`, `tenor_angsuran`, `status`) VALUES
(1, 1, '2019-05-10', 2000000, 15, 2, 1),
(2, 2, '2019-05-10', 5000000, 15, 4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_role`
--

CREATE TABLE `tbl_role` (
  `id` int(6) NOT NULL,
  `rolename` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_role`
--

INSERT INTO `tbl_role` (`id`, `rolename`) VALUES
(1, 'Administrator'),
(2, 'Anggota'),
(3, 'Owner');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_simpan`
--

CREATE TABLE `tbl_simpan` (
  `id` int(6) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `bayar_id` int(11) DEFAULT NULL,
  `tgl_simpan` date NOT NULL,
  `nominal_simpan` int(15) NOT NULL,
  `metode` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_simpan`
--

INSERT INTO `tbl_simpan` (`id`, `id_anggota`, `bayar_id`, `tgl_simpan`, `nominal_simpan`, `metode`, `status`) VALUES
(1, 1, NULL, '2019-05-08', 100000, 1, 1),
(2, 1, 4, '2019-05-08', 50000, 2, 1),
(3, 2, 5, '2019-05-08', 500000, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tenor`
--

CREATE TABLE `tbl_tenor` (
  `id` int(11) NOT NULL,
  `tenor` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_tenor`
--

INSERT INTO `tbl_tenor` (`id`, `tenor`, `jumlah`, `status`) VALUES
(1, '6 Bulan', 6, 1),
(2, '1 Tahun', 12, 1),
(3, '2 Tahun', 24, 1),
(4, '3 Tahun', 36, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `role_id`, `nama`, `email`, `password`, `foto`) VALUES
(1, 1, 'Administrator', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'gbr.jpg'),
(3, 1, 'Erna Pujastuti', 'erna_puja@gmail.com', '035b3c6377652bd7d49b5d2e9a53ef40', 'img9.jpg'),
(4, 3, 'Jajang Nurjaman', 'jajang@gmail.com', 'b56b57039c86f8626ece5a1a35f86175', 'img1.jpg'),
(7, 2, 'Susi Handayani', 'susi@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'img8.jpg'),
(11, 2, 'Rudi Hartono', 'rudi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'img5.jpg'),
(12, 2, 'Asri Nur Ajizah', 'asi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'img3.jpg'),
(13, 3, 'owner', 'owner@gmail.com', '72122ce96bfec66e2396d2e25225d70a', 'WhatsApp Image 2019-05-08 at 2.40.47 PM.jpeg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_anggota`
--
ALTER TABLE `tbl_anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_angsuran`
--
ALTER TABLE `tbl_angsuran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_bayar`
--
ALTER TABLE `tbl_bayar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pinjam`
--
ALTER TABLE `tbl_pinjam`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_simpan`
--
ALTER TABLE `tbl_simpan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_tenor`
--
ALTER TABLE `tbl_tenor`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_anggota`
--
ALTER TABLE `tbl_anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_angsuran`
--
ALTER TABLE `tbl_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_bayar`
--
ALTER TABLE `tbl_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_pinjam`
--
ALTER TABLE `tbl_pinjam`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_role`
--
ALTER TABLE `tbl_role`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_simpan`
--
ALTER TABLE `tbl_simpan`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_tenor`
--
ALTER TABLE `tbl_tenor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
