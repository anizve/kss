<?php
if ($_SESSION['level']==1) 
{
	?>
	<!-- nav-item -->
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtanggota' OR $_GET['hal']=='addanggota' OR $_GET['hal']=='updanggota'){echo ' active';}?>">
	    <i class="icon ion-person-stalker"></i>
	    <span>Anggota</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtanggota" class="nav-link<?php if($_GET['hal']=='dtanggota'){echo ' active';}?>">Data Anggota</a></li>
	    <li class="nav-item"><a href="?hal=addanggota" class="nav-link<?php if($_GET['hal']=='addanggota'){echo ' active';}?>">Tambah Anggota</a></li>
	  </ul>
	</li>
	<!-- nav-item -->
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtsimpan' OR $_GET['hal']=='addsimpan' OR $_GET['hal']=='updsimpan'){echo ' active';}?>">
	    <i class="icon ion-arrow-shrink"></i>
	    <span>Simpan</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtsimpan" class="nav-link<?php if($_GET['hal']=='dtsimpan'){echo ' active';}?>">Data Simpan</a></li>
	    <li class="nav-item"><a href="?hal=addsimpan" class="nav-link<?php if($_GET['hal']=='addsimpan'){echo ' active';}?>">Tambah Simpan</a></li>
	  </ul>
	</li>
	<!-- nav-item -->
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtpinjam' OR $_GET['hal']=='addpinjam' OR $_GET['hal']=='updpinjam'){echo ' active';}?>">
	    <i class="icon ion-arrow-expand"></i>
	    <span>Pinjam</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtpinjam" class="nav-link<?php if($_GET['hal']=='dtpinjam'){echo ' active';}?>">Data Pinjam</a></li>
	    <li class="nav-item"><a href="?hal=addpinjam" class="nav-link<?php if($_GET['hal']=='addpinjam'){echo ' active';}?>">Tambah Pinjam</a></li>
	  </ul>
	</li>
	<!-- nav-item -->
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtangsuran' OR $_GET['hal']=='addangsuran' OR $_GET['hal']=='updangsuran'){echo ' active';}?>">
	    <i class="icon ion-compose"></i>
	    <span>Angsuran</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtangsuran" class="nav-link<?php if($_GET['hal']=='dtangsuran'){echo ' active';}?>">Data Angsuran</a></li>
	    <li class="nav-item"><a href="?hal=addangsuran" class="nav-link<?php if($_GET['hal']=='addangsuran'){echo ' active';}?>">Tambah Angsuran</a></li>
	  </ul>
	</li>
	<!-- nav-item -->	
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtusers' OR $_GET['hal']=='addusers' OR $_GET['hal']=='updusers'){echo ' active';}?>">
	    <i class="icon ion-person-add"></i>
	    <span>Pengguna</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtpengguna" class="nav-link<?php if($_GET['hal']=='dtusers'){echo ' active';}?>">Data Pengguna</a></li>
	    <li class="nav-item"><a href="?hal=addpengguna" class="nav-link<?php if($_GET['hal']=='addusers'){echo ' active';}?>">Tambah Pengguna</a></li>
	  </ul>
	</li>
	<!-- nav-item -->
	<?php
}
elseif ($_SESSION['level']==2)
{
	?>
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtsimpan' OR $_GET['hal']=='addsimpan' OR $_GET['hal']=='updsimpan'){echo ' active';}?>">
	    <i class="icon ion-arrow-shrink"></i>
	    <span>Simpan</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtsimpan" class="nav-link<?php if($_GET['hal']=='dtsimpan'){echo ' active';}?>">Data Simpan</a></li>
	    <li class="nav-item"><a href="?hal=addsimpan" class="nav-link<?php if($_GET['hal']=='addsimpan'){echo ' active';}?>">Tambah Simpan</a></li>
	  </ul>
	</li>
	<!-- nav-item -->
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtpinjam' OR $_GET['hal']=='addpinjam' OR $_GET['hal']=='updpinjam'){echo ' active';}?>">
	    <i class="icon ion-arrow-expand"></i>
	    <span>Pinjam</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtpinjam" class="nav-link<?php if($_GET['hal']=='dtpinjam'){echo ' active';}?>">Data Pinjam</a></li>
	    <li class="nav-item"><a href="?hal=addpinjam" class="nav-link<?php if($_GET['hal']=='addpinjam'){echo ' active';}?>">Tambah Pinjam</a></li>
	  </ul>
	</li>
	<!-- nav-item -->
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtangsuran' OR $_GET['hal']=='addangsuran' OR $_GET['hal']=='updangsuran'){echo ' active';}?>">
	    <i class="icon ion-compose"></i>
	    <span>Angsuran</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtangsuran" class="nav-link<?php if($_GET['hal']=='dtangsuran'){echo ' active';}?>">Data Angsuran</a></li>
	    <li class="nav-item"><a href="?hal=addangsuran" class="nav-link<?php if($_GET['hal']=='addangsuran'){echo ' active';}?>">Tambah Angsuran</a></li>
	  </ul>
	</li>
	<?php
}
elseif ($_SESSION['level']==3)
{
	?>
	<li class="nav-item">
	  <a href="" class="nav-link with-sub<?php if($_GET['hal']=='dtlaporan' OR $_GET['hal']=='addlaporan' OR $_GET['hal']=='updlaporan'){echo ' active';}?>">
	    <i class="icon ion-stats-bars"></i>
	    <span>Laporan</span>
	  </a>
	  <ul class="nav-sub">
	    <li class="nav-item"><a href="?hal=dtlaporan" class="nav-link<?php if($_GET['hal']=='dtlaporan'){echo ' active';}?>">Data Laporan</a></li>
	    <li class="nav-item"><a href="?hal=addlaporan" class="nav-link<?php if($_GET['hal']=='addlaporan'){echo ' active';}?>">Tambah Laporan</a></li>
	  </ul>
	</li>
	<!-- nav-item -->
	<?php
}
else
{
	echo "Tidak Ada Menu Tampil";
}?>





