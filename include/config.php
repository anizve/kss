<?php
include "include/db.php";
if ($_GET['hal']=='forgot_password') 
{
	include 'forgot_password.php';
}
if ($_GET['hal'] == 'logout') 
{
	unset($_SESSION['email']);
	unset($_SESSION['level']);
	unset($_SESSION['nama']);
	unset($_SESSION['id']);

	session_destroy();
	echo "<script>window.location = '?hal=main';</script>";
}
if ($_GET['hal']=='profile') 
{
	$title = "Halaman Profile";
	$icon = "ion-person";
	$bc = '<span class="breadcrumb-item active">Halaman Utama</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'profile.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='main') 
{
	$title = "Halaman Utama";
	$icon = "ion-ios-home";
	$bc = '<span class="breadcrumb-item active">Halaman Utama</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'main.php';
		}
		if ($act == 'foot') 
		{
			?>
			<script src="lib/jquery-ui/jquery-ui.js"></script>
			<script src="lib/moment/moment.js"></script>
		    <script src="lib/Flot/jquery.flot.js"></script>
		    <script src="lib/Flot/jquery.flot.resize.js"></script>
		    <script src="lib/flot-spline/jquery.flot.spline.js"></script>
		    <script src="js/dashboard.js"></script>
			<?php
		}
	}
}
// ######################### Anggota ######################## 
if ($_GET['hal']=='dtanggota') 
{
	$title = "Halaman Data Anggota";
	$icon = "ion-person-stalker";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Data Anggota</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>
			<link href="lib/datatables/jquery.dataTables.css" rel="stylesheet">
    		<link href="lib/select2/css/select2.min.css" rel="stylesheet">
			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/dt_anggota.php';
		}
		if ($act == 'foot') 
		{
			?>
			<script src="lib/datatables/jquery.dataTables.js"></script>
		    <script src="lib/datatables-responsive/dataTables.responsive.js"></script>
		    <script src="lib/select2/js/select2.min.js"></script>
			<script>
		      $(function() {
		        'use strict';

		        $('#datatable1').DataTable({
		          responsive: true,
		          language: {
		            searchPlaceholder: 'Cari...',
		            sSearch: '',
		            lengthMenu: '_MENU_ items/page',
		          }
		        });

		        // Select2
		        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

		      });
		    </script>
			<?php
		}
	}
}
if ($_GET['hal']=='addanggota') 
{
	$title = "Halaman Tambah Anggota";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Tambah Anggota</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/add_anggota.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='updanggota') 
{
	$title = "Halaman Edit Anggota";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Edit Anggota</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/upd_anggota.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='delanggota') 
{
	$id = $_GET['id'];
	$qdel = mysqli_query($kon, "DELETE FROM `tbl_anggota` WHERE `id` = $id");
	if ($qdel) 
	{
		echo "<script>window.location = '?hal=dtanggota&error=0';</script>";
	}
	else
	{
		echo "<script>window.location = '?hal=dtanggota&error=1';</script>";
	}
}
// ######################### Simpan ######################## 
if ($_GET['hal']=='dtsimpan') 
{
	$title = "Halaman Data Simpan";
	$icon = "ion-person-stalker";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Data Simpan</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>
			<link href="lib/datatables/jquery.dataTables.css" rel="stylesheet">
    		<link href="lib/select2/css/select2.min.css" rel="stylesheet">
			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/dt_simpan.php';
		}
		if ($act == 'foot') 
		{
			?>
			<script src="lib/datatables/jquery.dataTables.js"></script>
		    <script src="lib/datatables-responsive/dataTables.responsive.js"></script>
		    <script src="lib/select2/js/select2.min.js"></script>
			<script>
		      $(function() {
		        'use strict';

		        $('#datatable1').DataTable({
		          responsive: true,
		          language: {
		            searchPlaceholder: 'Cari...',
		            sSearch: '',
		            lengthMenu: '_MENU_ items/page',
		          }
		        });

		        // Select2
		        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

		      });
		    </script>
			<?php
		}
	}
}
if ($_GET['hal']=='addsimpan') 
{
	$title = "Halaman Tambah Simpan";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Tambah Simpan</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>
			<link href="lib/select2/css/select2.min.css" rel="stylesheet">
			<link href="lib/spectrum/spectrum.css" rel="stylesheet">
			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/add_simpan.php';
		}
		if ($act == 'foot') 
		{
			?>
			<script src="lib/select2/js/select2.min.js"></script>
			<script src="lib/spectrum/spectrum.js"></script>
			<script>
		      $(function(){

		        'use strict';

		        $('.select2').select2({
		          minimumResultsForSearch: Infinity
		        });

		        // Select2 by showing the search
		        $('.select2-show-search').select2({
		          minimumResultsForSearch: ''
		        });

		      });
		    </script>
			<?php
		}
	}
}
if ($_GET['hal']=='updsimpan') 
{
	$title = "Halaman Edit Simpan";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Edit Simpan</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/upd_simpan.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='delsimpan') 
{
	$id = $_GET['id'];
	$qdel = mysqli_query($kon, "DELETE FROM `tbl_simpan` WHERE `id` = $id");
	if ($qdel) 
	{
		echo "<script>window.location = '?hal=dtsimpan&error=0';</script>";
	}
	else
	{
		echo "<script>window.location = '?hal=dtsimpan&error=1';</script>";
	}
}
if ($_GET['hal']=='accepted') 
{
	$id = $_GET['id'];
	$qdel = mysqli_query($kon, "UPDATE `tbl_simpan` SET `status` = '1' WHERE `tbl_simpan`.`id` = $id");
	if ($qdel) 
	{
		echo "<script>window.location = '?hal=dtsimpan&error=0';</script>";
	}
	else
	{
		echo "<script>window.location = '?hal=dtsimpan&error=1';</script>";
	}
}
if ($_GET['hal']=='canceled') 
{
	$id = $_GET['id'];
	$qdel = mysqli_query($kon, "UPDATE `tbl_simpan` SET `status` = '2' WHERE `tbl_simpan`.`id` = $id");
	if ($qdel) 
	{
		echo "<script>window.location = '?hal=dtsimpan&error=0';</script>";
	}
	else
	{
		echo "<script>window.location = '?hal=dtsimpan&error=1';</script>";
	}
}
// ######################### Pinjam ######################## 
if ($_GET['hal']=='dtpinjam') 
{
	$title = "Halaman Data Pinjam";
	$icon = "ion-person-stalker";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Data Pinjam</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>
			<link href="lib/datatables/jquery.dataTables.css" rel="stylesheet">
    		<link href="lib/select2/css/select2.min.css" rel="stylesheet">
			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/dt_pinjam.php';
		}
		if ($act == 'foot') 
		{
			?>
			<script src="lib/datatables/jquery.dataTables.js"></script>
		    <script src="lib/datatables-responsive/dataTables.responsive.js"></script>
		    <script src="lib/select2/js/select2.min.js"></script>
			<script>
		      $(function() {
		        'use strict';

		        $('#datatable1').DataTable({
		          responsive: true,
		          language: {
		            searchPlaceholder: 'Cari...',
		            sSearch: '',
		            lengthMenu: '_MENU_ items/page',
		          }
		        });

		        // Select2
		        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

		      });
		    </script>
			<?php
		}
	}
}
if ($_GET['hal']=='addpinjam') 
{
	$title = "Halaman Tambah Pinjam";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Tambah Pinjam</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/add_pinjam.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='updpinjam') 
{
	$title = "Halaman Edit Pinjam";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Edit Pinjam</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/upd_pinjam.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='stpinjam') 
{
	$id = $_GET['id'];
	$st = $_GET['st'];
	$qna = mysqli_query($kon, "UPDATE `tbl_pinjam` SET `status` = '$st' WHERE `id` = $id");
	if ($qna) 
	{
		echo "<script>window.location = '?hal=dtpinjam&error=0';</script>";
	}
	else
	{
		echo "<script>window.location = '?hal=dtpinjam&error=1';</script>";
	}
}
if ($_GET['hal']=='delpinjam') 
{
	$id = $_GET['id'];
	$qdel = mysqli_query($kon, "DELETE FROM `tbl_pinjam` WHERE `id` = $id");
	if ($qdel) 
	{
		echo "<script>window.location = '?hal=dtpinjam&error=0';</script>";
	}
	else
	{
		echo "<script>window.location = '?hal=dtpinjam&error=1';</script>";
	}
}
// ######################### Angsuran ######################## 
if ($_GET['hal']=='angsuranagt') 
{
	$title = "Halaman Tambah Angsuran";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Tambah Angsuran</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/angsuran_anggota.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='dtangsuran') 
{
	$title = "Halaman Data Angsuran";
	$icon = "ion-person-stalker";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Data Angsuran</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>
			<link href="lib/datatables/jquery.dataTables.css" rel="stylesheet">
    		<link href="lib/select2/css/select2.min.css" rel="stylesheet">
			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/dt_angsuran.php';
		}
		if ($act == 'foot') 
		{
			?>
			<script src="lib/datatables/jquery.dataTables.js"></script>
		    <script src="lib/datatables-responsive/dataTables.responsive.js"></script>
		    <script src="lib/select2/js/select2.min.js"></script>
			<script>
		      $(function() {
		        'use strict';

		        $('#datatable1').DataTable({
		          responsive: true,
		          language: {
		            searchPlaceholder: 'Cari...',
		            sSearch: '',
		            lengthMenu: '_MENU_ items/page',
		          }
		        });

		        // Select2
		        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

		      });
		    </script>
			<?php
		}
	}
}
if ($_GET['hal']=='addangsuran') 
{
	$title = "Halaman Tambah Angsuran";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Tambah Angsuran</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/add_angsuran.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='updangsuran') 
{
	$title = "Halaman Edit Angsuran";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Edit Angsuran</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/upd_angsuran.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='delangsuran') 
{
	$id = $_GET['id'];
	$qdel = mysqli_query($kon, "DELETE FROM `tbl_angsuran` WHERE `id` = $id");
	if ($qdel) 
	{
		echo "<script>window.location = '?hal=dtangsuran&error=0';</script>";
	}
	else
	{
		echo "<script>window.location = '?hal=dtangsuran&error=1';</script>";
	}
}
// ######################### Pengguna ######################## 
if ($_GET['hal']=='dtpengguna') 
{
	$title = "Halaman Data Pengguna";
	$icon = "ion-person-stalker";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Data Pengguna</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>
			<link href="lib/datatables/jquery.dataTables.css" rel="stylesheet">
    		<link href="lib/select2/css/select2.min.css" rel="stylesheet">
			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/dt_pengguna.php';
		}
		if ($act == 'foot') 
		{
			?>
			<script src="lib/datatables/jquery.dataTables.js"></script>
		    <script src="lib/datatables-responsive/dataTables.responsive.js"></script>
		    <script src="lib/select2/js/select2.min.js"></script>
			<script>
		      $(function() {
		        'use strict';

		        $('#datatable1').DataTable({
		          responsive: true,
		          language: {
		            searchPlaceholder: 'Cari...',
		            sSearch: '',
		            lengthMenu: '_MENU_ items/page',
		          }
		        });

		        // Select2
		        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

		      });
		    </script>
			<?php
		}
	}
}
if ($_GET['hal']=='addpengguna') 
{
	$title = "Halaman Tambah Pengguna";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Tambah Pengguna</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/add_pengguna.php';
		}
		if ($act == 'foot') 
		{
			?>
			
			<?php
		}
	}
}
if ($_GET['hal']=='updpengguna') 
{
	$title = "Halaman Edit Pengguna";
	$icon = "ion-person-add";
	$bc = '<a class="breadcrumb-item" href="?hal=main">Halaman Utama</a>
          <span class="breadcrumb-item active">Halaman Edit Pengguna</span>';

	function Media($act)
	{
		if ($act == 'head') 
		{
			?>

			<?php
		}
		if ($act == 'isi') 
		{
			include 'app/upd_pengguna.php';
		}
		if ($act == 'foot') 
		{
			?>

			<?php
		}
	}
}
if ($_GET['hal']=='delpengguna') 
{
	$id = $_GET['id'];
	$qdel = mysqli_query($kon, "DELETE FROM `tbl_users` WHERE `id` = $id");
	if ($qdel) 
	{
		echo "<script>window.location = '?hal=dtpengguna&error=0';</script>";
	}
	else
	{
		echo "<script>window.location = '?hal=dtpengguna&error=1';</script>";
	}
}
?>