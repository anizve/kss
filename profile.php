<?php 
include 'include/db.php';
$id = $_SESSION['id'];
$que = mysqli_query($kon, "SELECT * FROM `tbl_users` WHERE `id` = $id");
$dta = mysqli_fetch_array($que);
?>

<div class="row row-sm">
  <div class="col-3">
    <div class="card bd-primary">
      <div class="card-header bg-primary tx-white">
        Profile Photo
      </div><!-- card-header -->
      <div class="card-body tx-center">
        <img src="img/<?php echo $dta['foto'];?>" class="wd-100 rounded-circle" alt="">
        <div class="mg-t-20">
          <a href="" class="btn btn-primary pd-x-20 mg-r-2">Edit</a>
        </div>
      </div><!-- card-body -->
    </div><!-- card -->

    <div class="card bd-primary mg-t-20">
      <div class="card-header bg-primary tx-white">
        Skills
      </div><!-- card-header -->
      <div class="list-group list-group-flush list-group-skills">
        <div class="list-group-item">
          <span>HTML5</span>
          <a href="">&times;</a>
        </div><!-- list-group-item -->
        <div class="list-group-item">
          <span>CSS3</span>
          <a href="">&times;</a>
        </div><!-- list-group-item -->
        <div class="list-group-item">
          <span>Javascript</span>
          <a href="">&times;</a>
        </div><!-- list-group-item -->
        <div class="list-group-item">
          <span>PHP</span>
          <a href="">&times;</a>
        </div><!-- list-group-item -->
        <div class="list-group-item">
          <span>Photoshop</span>
          <a href="">&times;</a>
        </div><!-- list-group-item -->
        <div class="list-group-item">
          <span>Bootstrap</span>
          <a href="">&times;</a>
        </div><!-- list-group-item -->
        <div class="list-group-item">
          <span>Angular</span>
          <a href="">&times;</a>
        </div><!-- list-group-item -->
        <div class="list-group-item">
          <span>React</span>
          <a href="">&times;</a>
        </div><!-- list-group-item -->
      </div><!-- list-group -->
    </div><!-- card -->

    <div class="card bd-primary mg-t-20">
      <div class="card-header bg-primary tx-white">Notifications</div>
      <div class="card-body">
        <div class="d-flex">
          <div class="ckbox mg-t-2 mg-r-15">
            <input type="checkbox">
            <span></span>
          </div><!-- ckbox -->
          <span>Email me when someone mentions me.</span>
        </div>

        <div class="d-flex mg-t-10">
          <div class="ckbox mg-t-2 mg-r-15">
            <input type="checkbox">
            <span></span>
          </div><!-- ckbox -->
          <span>Email me when someone follows me.</span>
        </div>
      </div><!-- card-body -->
    </div><!-- card -->

  </div><!-- col-3 -->
  <div class="col-9">

    <div class="card bd-primary">
      <div class="card-header bg-primary tx-white">
        Login Information
      </div><!-- card-header -->
      <div class="card-body">
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Email</label>
          <div class="col-6">
            <input type="email" class="form-control" placeholder="Enter email" value="<?php echo $dta['email'];?>" disabled>
          </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row align-items-center mg-b-0">
          <label class="mg-b-0 col-3 tx-right">Password</label>
          <div class="col-6">
            <input type="password" class="form-control" placeholder="Password">
          </div><!-- col-8 -->
        </div><!-- form-group -->
        
      </div><!-- card-body -->
    </div><!-- card -->

    <div class="card bd-primary mg-t-20">
      <div class="card-header bg-primary tx-white">
        Personal Information
      </div><!-- card-header -->
      <div class="card-body">
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Firstname</label>
          <div class="col-6">
            <input type="text" class="form-control" placeholder="Enter firstname" value="John">
          </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Lastname</label>
          <div class="col-6">
            <input type="text" class="form-control" placeholder="Enter lastname" value="Doe">
          </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Location</label>
          <div class="col-6">
            <input type="text" class="form-control" placeholder="Enter location" value="San Francisco, CA">
          </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Portfolio URL</label>
          <div class="col-6">
            <input type="text" class="form-control" placeholder="Enter URL" value="http://themepixels.me">
          </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row">
          <label class="mg-b-0 col-3 tx-right mg-t-10">About You</label>
          <div class="col-8">
            <textarea class="form-control" rows="4" placeholder="Write some brief description about you..."></textarea>
          </div><!-- col-8 -->
        </div><!-- form-group -->
      </div><!-- card-body -->
    </div><!-- card -->

    <div class="card bd-primary mg-t-20">
      <div class="card-header bg-primary tx-white">
        Your External Links
      </div><!-- card-header -->
      <div class="card-body">
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Facebook</label>
          <div class="col-6">
            <input type="text" class="form-control" placeholder="Paste your facebook URL here">
          </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Twitter</label>
          <div class="col-6">
            <input type="text" class="form-control" placeholder="Paste your twitter URL here">
          </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Github</label>
          <div class="col-6">
            <input type="text" class="form-control" placeholder="Paste your github URL here">
          </div><!-- col-8 -->
        </div><!-- form-group -->
        <div class="form-group row align-items-center">
          <label class="mg-b-0 col-3 tx-right">Linkedin</label>
          <div class="col-6">
            <input type="text" class="form-control" placeholder="Paste your linkedin URL here">
          </div><!-- col-8 -->
        </div><!-- form-group -->
      </div><!-- card-body -->
    </div><!-- card -->
  </div><!-- col-9 -->
</div><!-- row -->

<hr class="mg-y-30">

<button class="btn btn-success pd-x-20 mg-r-2">Save Changes</button>
<button class="btn btn-secondary pd-x-20">Deactivate My Account</button>

<hr class="mg-t-30">
