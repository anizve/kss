<!DOCTYPE html>
<html lang="id">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title>.:: Halaman Login | Koperasi Kredit Usaha Sejahtera ::.</title>

    <!-- Vendor css -->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Shamcey CSS -->
    <link rel="stylesheet" href="css/shamcey.css">
  </head>

  <body class="bg-gray-900">

    <div class="signpanel-wrapper">
      <div class="signbox">
        <div class="signbox-header">
          <h2>Halaman Login</h2>
          <p class="mg-b-0">Koperasi Kredit Usaha Sejahtera</p>
        </div><!-- signbox-header -->
        <div class="signbox-body">
          <form action="" method="post" id="login">
            <div class="form-group">
              <label class="form-control-label">Email Anda:</label>
              <input type="email" name="email" placeholder="Masukan Alamat Email" class="form-control">
            </div><!-- form-group -->
            <div class="form-group">
              <label class="form-control-label">Sandi Anda:</label>
              <input type="password" name="pass" placeholder="Masukan Kata Sandi" class="form-control">
            </div><!-- form-group -->
            <div class="form-group">
              <a href="?hal=forgot_password">Lupa Sandi Anda?</a>
            </div><!-- form-group -->
            <button class="btn btn-primary btn-block" name="login">Login</button>
          </form>
          <?php
          include 'include/db.php';
          if (isset($_POST['login'])) 
          {
            $un = $_POST['email'];
            $ps = $_POST['pass'];

            $sql = "SELECT * FROM `tbl_users` WHERE `email` = '$un' AND `password` = md5('$ps')";
            $que = mysqli_query($kon, $sql);
            $cek = mysqli_num_rows($que);
            //echo $cek;
            if ($cek == 1) 
            {
              $data = mysqli_fetch_array($que);
              $_SESSION['email'] = $data['email'];
              $_SESSION['id'] = $data['id'];
              $_SESSION['nama'] = $data['nama'];
              $_SESSION['level'] = $data['role_id'];
              $_SESSION['foto'] = $data['foto'];

              $idu = $_SESSION['id'];
              if ($_SESSION['level'] == 2) 
              {
                $sql1 = "SELECT * FROM `tbl_anggota` WHERE `id_users` = '$idu'";
                $que1 = mysqli_query($kon, $sql1);
                $dta1 = mysqli_fetch_array($que1);
                $_SESSION['ida'] = $dta1['id'];
              }

              echo "<script>window.location = '?hal=main';</script>";
            }
            else
            {
              echo "Login Gagal";
            }
          }
          ?>
        </div><!-- signbox-body -->
      </div><!-- signbox -->
    </div><!-- signpanel-wrapper -->

    <script src="lib/jquery/jquery.js"></script>
    <script src="lib/popper.js/popper.js"></script>
    <script src="lib/bootstrap/bootstrap.js"></script>

    <script src="js/shamcey.js"></script>
  </body>
</html>
