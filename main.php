<div class="row row-sm">
  <div class="col-lg-8">
    <div class="row row-xs">
      <div class="col-6 col-sm-4 col-md">
        <a href="#" class="shortcut-icon">
          <div>
            <i class="icon ion-person-stalker"></i>
            <span>Anggota</span>
          </div>
        </a>
      </div><!-- col -->
      <div class="col-6 col-sm-4 col-md">
        <a href="#" class="shortcut-icon">
          <div>
            <i class="icon ion-arrow-shrink"></i>
            <span>Simpan</span>
          </div>
        </a>
      </div><!-- col -->
      <div class="col-6 col-sm-4 col-md mg-t-10 mg-sm-t-0">
        <a href="#" class="shortcut-icon">
          <div>
            <i class="icon ion-arrow-expand"></i>
            <span>Pinjam</span>
          </div>
        </a>
      </div><!-- col -->
      <div class="col-6 col-sm-4 col-md mg-t-10 mg-md-t-0">
        <a href="#" class="shortcut-icon">
          <div>
            <i class="icon ion-compose"></i>
            <span>Angsuran</span>
          </div>
        </a>
      </div><!-- col -->
      <div class="col-6 col-sm-4 col-md mg-t-10 mg-md-t-0">
        <a href="#" class="shortcut-icon">
          <div>
            <i class="icon ion-stats-bars"></i>
            <span>Laporan</span>
          </div>
        </a>
      </div><!-- col -->
    </div><!-- row -->

    <div class="card bd-primary mg-t-20">
      <div class="card-header bg-primary tx-white">Grafik Simpan Pinjam</div>
      <div class="card-body">
        <div id="flotArea" class="ht-200 ht-sm-300"></div>
      </div><!-- card-body -->
    </div><!-- card -->

  </div><!-- col-8 -->
  <div class="col-lg-4 mg-t-20 mg-lg-t-0">
    <div class="card bd-primary">
      <div class="card-header bg-primary tx-white">Prakata</div>
      <div class="card-body">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      </div><!-- card-body -->
    </div><!-- card -->

    <div class="card bd-primary card-calendar mg-t-20">
      <div class="card-header bg-primary tx-white">Kalender</div>
      <div class="datepicker"></div>
    </div><!-- card -->
  </div><!-- col-4 -->
</div><!-- row -->
