<?php
include 'include/db.php';
$id = $_GET['idp'];
$que = mysqli_query($kon, "SELECT * FROM `tbl_pinjam` WHERE `id` = $id");
$dta = mysqli_fetch_array($que);

?>
<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Full Color Variations for Table</div>
  <div class="card-body pd-sm-30">
    <p class="mg-b-20 mg-sm-b-30">Detail Pembayaran Pinjaman.</p>
    
    <table width="100%" border="0" class="table table-hover">
      <tr>
        <td class="wd-20p"><b>Nama Lengkap</b></td>
        <td>:</td>
        <td><?php
        $ida2 = $dta['id_anggota'];
        $que2 = mysqli_query($kon, "SELECT * FROM `tbl_anggota` WHERE `id` = $ida2");
        $dta2 = mysqli_fetch_array($que2);
        echo $dta2['nama_lengkap'];
        ?></td>
        <td>&nbsp;</td>
        <td class="wd-20p"><b>Bunga</b></td>
        <td>:</td>
        <td><?php echo $dta['bunga_pinjam'];?>%</td>
      </tr>
      <tr>
        <td class="wd-20p"><b>Tanggal Pinjam</b></td>
        <td>:</td>
        <td><?php echo tgl_indo($dta['tgl_pinjam']);?></td>
        <td>&nbsp;</td>
        <td class="wd-20p"><b>Tenor</b></td>
        <td>:</td>
        <td><?php
          $tn = $dta['tenor_angsuran'];
          $que3 = mysqli_query($kon, "SELECT * FROM `tbl_tenor` WHERE `id` = $tn");
          $dta3 = mysqli_fetch_assoc($que3);
          $jml = $dta3['jumlah'];
          $tnr = $dta3['tenor'];
          echo $tnr;
          ?></td>
      </tr>
      <tr>
        <td class="wd-20p"><b>Nominal</b></td>
        <td>:</td>
        <td><?php echo rupiah($dta['nominal_pinjam']);?></td>
        <td>&nbsp;</td>
        <td class="wd-20p"><b>Status</b></td>
        <td>:</td>
        <td><?php
              if ($dta['status']==0) 
              {
                echo '<font color="#ffbf00">Dalam Proses</font>';
              }
              elseif ($dta['status']==1) 
              {
                echo '<font color="#008000">Disetuji</font>';
              }
              else
              {
                echo '<font color="#ff0000">Tidak Disetuji</font>';
              }
            ?></td>
      </tr>

    </table>
    
    <div class="table-responsive">
      <table class="table table-hover table-bordered table-primary mg-b-0">
        <thead>
          <tr>
            <th>NO</th>
            <th>PEMBAYARAN KE</th>
            <th>TANGGAL JATUH TEMPO</th>
            <th>NOMINAL</th>
            <th>STATUS</th>
            <th>AKSI</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $h1 = $dta['nominal_pinjam'];
          $h2 = $h1*$dta['bunga_pinjam']/100;
          $h3 = $h1/$jml;
          $h4 = $h3+$h2;
          for ($i=1; $i <= $jml; $i++) 
          { 
            # code...
          ?>          
          <tr>
            <td><?php echo $i;?></td>
            <td>Bulan Ke <?php echo $i;?></td>
            <td>
              <?php
              $tpj = $dta['tgl_pinjam'];
              $tg1 = substr($tpj, 0,4);
              $tg2 = substr($tpj, 5,2);
              $tg3 = substr($tpj, -2);
              
              $tg4 = $tg1.'-'.$tg2++.'-'.$tg3;
              for ($tt=$tg2lg ; $tt > $tg2 ; $tt++) 
              { 
              echo $tg1;
              echo "-";
              echo $tt;
              echo "-";
              echo $tg3;
              }
              //$tjt = date('Y-m-d', strtotime('+1 month', strtotime( $tpj )));
              //echo tgl_indo($tjt);
              ?>
            </td>
            <td><?php echo rupiah($h4);?></td>
            <td>
              Belum Dibayar
            </td>
            <td><a href="?hal=bayarangusran" class="btn btn-primary">Bayar</a></td>
          </tr>
          <?php
          }?>
        </tbody>
      </table>
    </div>
  </div>
</div>
