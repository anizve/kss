<?php
include 'include/db.php';
if ($_SESSION['level']!=1) 
{
    echo "<script>window.location = '?hal=main';</script>";
}
$id = $_GET['id'];
$que1 = mysqli_query($kon, "SELECT * FROM `tbl_users` WHERE `id` = $id");
$dta1 = mysqli_fetch_array($que1);
?>

<div class="row row-sm mg-t-20">
  <div class="col-xl-6">
    <div class="card bd-primary">
      <div class="card-header bg-primary tx-white">Edit Pengguna</div>
      <div class="card-body pd-sm-30 form-layout form-layout-5">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="row row-xs">
            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Nama:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="text" class="form-control" name="nm" value="<?php echo $dta1['nama'];?>">
            </div>
          </div>
          <br>
          <div class="row row-xs">
            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Email:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="email" class="form-control" name="ml" value="<?php echo $dta1['email'];?>">
            </div>
          </div>
          <br>
          <div class="row row-xs">
            <label class="col-sm-4 form-control-label" name="lvl"><span class="tx-danger">*</span> Hak Akses:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih Hak Akses" tabindex="-1" aria-hidden="true" name="lvl">
                <option>Pilih Hak Akses</option>
                <?php
                  $sql = "SELECT * FROM `tbl_role`";
                  $que = mysqli_query($kon, $sql);
                  while ($dta = mysqli_fetch_assoc($que)) 
                  {
                      echo '<option value="'.$dta['id'].'"';
                      if ($dta1['role_id']==$dta['id']) 
                      {
                        echo ' selected=""';
                      }
                      echo '>'.$dta['rolename'].`</optiion>`;
                  }
                 ?>
              </select>
            </div>
          </div>
          <br>
          <div class="row row-xs mg-t-30">
            <div class="col-sm-8 mg-l-auto">
              <div class="form-layout-footer">
                <button class="btn btn-primary mg-r-5" name="simpan">Simpan</button>
                <a href="?hal=dtpengguna" class="btn btn-danger">Batal</a>
              </div><!-- form-layout-footer -->
            </div><!-- col-8 -->
          </div>
        </form>
        <?php
          if (isset($_POST['simpan'])) 
          {
            $nama = $_POST['nm'];
            $mail = $_POST['ml'];
            $level = $_POST['lvl'];

            $que2 = mysqli_query($kon, "UPDATE `tbl_users` SET `role_id` = '$level', `nama` = '$nama', `email` = '$mail' WHERE `id` = $id");
            if ($que2) 
            {
                echo "<script>window.location = '?hal=dtpengguna';</script>";
            }
            else
            {
            ?>
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Terjadi Kesalahan!</strong> Gagal Proses Data Mohon ulangi.
                </div><!-- alert -->
            <?php
            }
          }
        ?>
      </div><!-- card-body -->
    </div><!-- card -->
  </div>
  <div class="col-xl-6 mg-t-25 mg-xl-t-0">
    <div class="card bd-primary">
      <div class="card-header bg-primary tx-white">Ubah Kata Sandi</div>
      <div class="card-body pd-sm-30 form-layout form-layout-5">
        <form action="" method="post">
          <div class="row row-xs">
            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Password Baru:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="password" class="form-control" name="pw" placeholder="Masukan Password Baru">
            </div>
          </div>
          
          <div class="row row-xs mg-t-30">
            <div class="col-sm-8 mg-l-auto">
              <div class="form-layout-footer">
                <button class="btn btn-primary mg-r-5" name="updpass">Simpan</button>
                <a href="?hal=dtpengguna" class="btn btn-danger">Batal</a>
              </div><!-- form-layout-footer -->
            </div><!-- col-8 -->
          </div>
        </form>
        <?php
          if (isset($_POST['updpass'])) 
          {
            $pwd = $_POST['pw'];

            $que3 = mysqli_query($kon, "UPDATE `tbl_users` SET `password` = MD5('$pwd') WHERE `id` = $id");
            if ($que3) 
            {
                echo "<script>window.location = '?hal=dtpengguna';</script>";
            }
            else
            {
            ?>
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Terjadi Kesalahan!</strong> Gagal Proses Data Mohon ulangi.
                </div><!-- alert -->
            <?php
            }
          }
        ?>
      </div>
    </div>
    <br>
    <div class="card bd-primary">
      <div class="card-header bg-primary tx-white">Edit Foto</div>
      <div class="card-body pd-sm-30 form-layout form-layout-5">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="row row-xs">
            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Pilih Foto:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="file" id="ft" name="fto" class="form-control" required="">
            </div>
          </div>
          
          <div class="row row-xs mg-t-30">
            <div class="col-sm-8 mg-l-auto">
              <div class="form-layout-footer">
                <button class="btn btn-primary mg-r-5" name="updfoto">Simpan</button>
                <a href="?hal=dtpengguna" class="btn btn-danger">Batal</a>
              </div><!-- form-layout-footer -->
            </div><!-- col-8 -->
          </div>
        </form>
        <?php
          if (isset($_POST['updfoto'])) 
          {
            $ext1   = array('png','jpg','jpeg','gif');
            $nft = $_FILES['fto']['name'];
            $x = explode('.', $nft);
            $ext2 = strtolower(end($x));
            $ukuran = $_FILES['fto']['size'];
            $file_tmp = $_FILES['fto']['tmp_name'];  

            if(in_array($ext2, $ext1) === true)
            {
                if($ukuran < 5242880)
                {           
                    move_uploaded_file($file_tmp, 'img/'.$nft);
                    $que5 = mysqli_query($kon, "UPDATE `tbl_users` SET `foto` = '$nft' WHERE `id` = $id");
                    if ($que5) 
                    {
                        echo "<script>window.location = '?hal=dtpengguna';</script>";
                    }
                    else
                    {
                    ?>
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong class="d-block d-sm-inline-block-force">Terjadi Kesalahan!</strong> Gagal Proses Data Mohon ulangi.
                        </div><!-- alert -->
                    <?php
                    }
                }
                else
                {
                    echo 'UKURAN FILE TERLALU BESAR';
                }
            }
            else
            {
                echo 'EXTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
            }
          }
        ?>
      </div>
    </div>
  </div>
</div>