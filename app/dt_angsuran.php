<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tabel Data angsuran</div>
  <div class="card-body pd-sm-30">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <a href="?hal=addangsuran" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus mg-r-10"></i> Tambah Data</a>
      </div><!-- col-sm -->
      <div class="col-sm-6 col-md-3 mg-t-20 mg-sm-t-0">
        <a href="cetak/pr_users.php" class="btn btn-success btn-block mg-b-10"><i class="fa fa-print mg-r-10"></i> Cetak Data</a>
      </div><!-- col-sm -->
    </div><!-- row -->
    <hr>
    <div class="table-wrapper">
      <table id="datatable1" class="table display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">No</th>
            <th class="wd-20p">Nama Anggota</th>
            <th class="wd-20p">Bayar</th>
            <th class="wd-15p">Nominal Angsuran</th>
            <th class="wd-10p">Tanggal Angsuran</th>
            <th class="wd-10p">Tanggal Jatuh Tempo</th>
            <th class="wd-10p">Status</th>
            <th class="wd-25p">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
            include "include/db.php";
            $i = 1;
            $query = mysqli_query($kon, "SELECT * FROM `tbl_angsuran` ORDER BY `id` DESC");
            while ($data = mysqli_fetch_assoc($query)) 
            {
              $id = $data['id'];
            ?>
          <tr>
            <td><?php echo $i++;?></td>
            <td></td>
            <td><?php echo $data['bayar_id'];?></td> 
            <td><?php echo $data['nominal_angsuran'];?></td>
            <td><?php echo $data['tgl_angsuran'];?></td>
            <td><?php echo $data['jatuh_tempo'];?></td>
            <td><?php echo $data['status_angsuran'];?></td>

            <td>        
              <a href="#" data-toggle="modal" data-target="#detail<?php echo $id;?>"><i class="icon ion-search"></i></a>  &nbsp;
              <a href="?hal=updanggota"><i class="icon ion-edit"></i></a>  &nbsp;
              <a href="?hal=delanggota" onclick="return confirm('Anda Yakin Menghapus?');"><i class="icon ion-close"></i></a>
            </td>
          </tr>
          
                    <!-- LARGE MODAL -->
          <div id="detail<?php echo $id;?>" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Detail Anggota</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20">
                  <font color="#FFFFFFF" class="mg-b-5">--------------------------------------------------------------------------------------------</font>
                  <div class="row">
                    <div class="col-sm-6">
                      kode : 
                    </div>
                    <div class="col-sm-6">
                      <?php echo $data['kode_angsuran'];?>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      Nama Anggota : 
                    </div>
                    <div class="col-sm-6">
                      <?php echo $data['pinjaman_id'];?>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      Bayar : 
                    </div>
                    <div class="col-sm-6">
                      <?php echo $data['bayar_id'];?>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      Nominal Angsuran : 
                    </div>
                    <div class="col-sm-6">
                      <?php echo $data['nominal_angsuran'];?>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      Tanggal Angsuran : 
                    </div>
                    <div class="col-sm-6">
                      <?php echo $data['tgl_angsuran'];?>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      Tanggal Jatuh Tempo : 
                    </div>
                    <div class="col-sm-6">
                      <?php echo $data['jatuh_tempo'];?>
                    </div>
                  </div>

                    <div class="row">
                    <div class="col-sm-6">
                      Status : 
                    </div>
                    <div class="col-sm-6">
                      <?php echo $data['status_angsuran'];?>
                    </div>
                  </div>

                </div><!-- modal-body -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Keluar</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

          <?php
           }
          ?>
        </tbody>
      </table>
    </div><!-- table-wrapper -->
  </div><!-- card-body -->
</div><!-- card -->