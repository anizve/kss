<?php
include 'include/db.php';
if ($_SESSION['level']!=1) 
{
    echo "<script>window.location = '?hal=main';</script>";
}
?>

<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tambah Pengguna</div>
  <div class="card-body pd-sm-30 form-layout form-layout-5">
    <form action="" method="post" enctype="multipart/form-data">
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Nama:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="text" class="form-control" placeholder="Masukan Nama" name="nm">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Email:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="email" class="form-control" placeholder="Masukan Email" name="ml">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Password:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="password" class="form-control" placeholder="Masukan Password" name="pwd">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Foto:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <label class="custom-file">
            <input type="file" id="file2" name="fto" class="custom-file-input">
            <span class="custom-file-control custom-file-control-primary"></span>
          </label>
      </div>
    </div>
    <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label" name="lvl"><span class="tx-danger">*</span> Hak Akses:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih Hak Akses" tabindex="-1" aria-hidden="true" name="lvl">
            <option>Pilih Hak Akses</option>
            <?php
              $sql = "SELECT * FROM `tbl_role`";
              $que = mysqli_query($kon, $sql);
              while ($dta = mysqli_fetch_assoc($que)) 
              {
                  echo '<option value="'.$dta['id'].'">'.$dta['rolename'].`</optiion>`;
              }
             ?>
          </select>
        </div>
      </div>
    <div class="row row-xs mg-t-30">
      <div class="col-sm-8 mg-l-auto">
        <div class="form-layout-footer">
          <button class="btn btn-success mg-r-5" name="simpan">Simpan</button>
          <a href="?hal=dtpengguna" class="btn btn-secondary">Batal</a>
        </div><!-- form-layout-footer -->
      </div><!-- col-8 -->
    </div>
  </form>
<?php
  if (isset($_POST['simpan'])) 
  {
      $nama = $_POST['nm'];
      $mail = $_POST['ml'];
      $pass = $_POST['pwd'];
      $nft = $_FILES['fto'];
      $level = $_POST['lvl'];

      $que4 = mysqli_query($kon, "SELECT * FROM `tbl_users` WHERE `email` = '$mail'");
      if (mysqli_num_rows($que4) == 1) 
      {
      ?>
          <div class="alert alert-warning" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <strong class="d-block d-sm-inline-block-force">Maaf!</strong> Username Telah Digunakan.
          </div><!-- alert -->
      <?php
      }
      else
      {
          $ext1   = array('png','jpg','jpeg','gif');
          $nft = $_FILES['fto']['name'];
          $x = explode('.', $nft);
          $ext2 = strtolower(end($x));
          $ukuran = $_FILES['fto']['size'];
          $file_tmp = $_FILES['fto']['tmp_name'];  

          if(in_array($ext2, $ext1) === true)
          {
              if($ukuran < 5242880)
              {           
                  move_uploaded_file($file_tmp, 'img/'.$nft);
                  $que5 = mysqli_query($kon, "INSERT INTO `tbl_users` (`role_id`, `nama`, `email`, `password`, `foto`) VALUES ('$level', '$nama', '$mail', md5('$pass'), '$nft')");
                  if ($que5) 
                  {
                      echo "<script>window.location = '?hal=dtpengguna';</script>";
                  }
                  else
                  {
                  ?>
                      <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                          <strong class="d-block d-sm-inline-block-force">Terjadi Kesalahan!</strong> Gagal Proses Data Mohon ulangi.
                      </div><!-- alert -->
                  <?php
                  }
              }
              else
              {
                  echo 'UKURAN FILE TERLALU BESAR';
              }
          }
          else
          {
              echo 'EXTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
          }
      }

  }
  ?>
  </div><!-- card-body -->
</div><!-- card -->