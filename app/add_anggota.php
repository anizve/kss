<?php
include 'include/db.php';
if ($_SESSION['level']!=1) 
{
    echo "<script>window.location = '?hal=main';</script>";
}
?>

<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tambah Anggota</div>
  <div class="card-body pd-sm-30 form-layout form-layout-5">
    <form action="" method="post" enctype="multipart/form-data">
      <p class="mg-b-20 mg-sm-b-30">Informasi Akun Anggota.</p>
      
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Email:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="email" class="form-control" placeholder="Masukan Email" name="ml">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Password:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="password" class="form-control" placeholder="Masukan Password" name="ps">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Foto:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <label class="custom-file">
            <input type="file" id="file2" name="ft" class="custom-file-input">
            <span class="custom-file-control custom-file-control-primary"></span>
          </label>
        </div>
      </div>
      
      <br><hr>
      <p class="mg-b-20 mg-sm-b-30">Informasi Data Pribadi.</p>
      <div class="row row-xs">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> NIK:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" placeholder="Masukan No Induk Kependudukan" name="nik">
        </div>
      </div><!-- row -->
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Nama Lengkap:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" placeholder="Masukan Nama Lengkap" name="nmlngkp">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Alamat:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <textarea rows="3" class="form-control" placeholder="Masukan Alamat" name="alm"></textarea>
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tempat Lahir:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" placeholder="Masukan Tempat Lahir" name="tmp_lhr">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal Lahir:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="date" class="form-control" name="tgl_lhr">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> No Telepon:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" placeholder="Masukan No Telepon" name="notel">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Pekerjaan:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <textarea rows="3" class="form-control" placeholder="Masukan Nama Pekejaan" name="pkrj"></textarea>
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Pendidikan:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih" tabindex="-1" aria-hidden="true" name="didik">
            <option label="Pilih"></option>
            <option value="1">SD</option>
            <option value="2">SMP</option>
            <option value="3">SMA</option>
            <option value="4">D1</option>
          </select>
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal Mulai Kerja:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="date" class="form-control" name="tmk">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Nama Suami/Istri:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" placeholder="Masukan Nama Suami/Istri" name="nm_si">
        </div>
      </div>
      
      <br>
      <hr>
      <p class="mg-b-20 mg-sm-b-30">Informasi saldo awal dan status.</p>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal Pendaftaran:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="date" class="form-control" name="mdk">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Saldo:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" placeholder="Masukan Jumlah Saldo" name="sld">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Status Anggota:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <label class="rdiobox">
            <input name="st" type="radio" value="0">
            <span>Pending</span>
          </label>
          <label class="rdiobox">
            <input name="st" type="radio" value="1">
            <span>Aktif</span>
          </label>
          <label class="rdiobox">
            <input name="st" type="radio" value="2">
            <span>Nonaktif</span>
          </label>
        </div>
      </div><!-- row -->
      <div class="row row-xs mg-t-30">
        <div class="col-sm-8 mg-l-auto">
          <div class="form-layout-footer">
            <button class="btn btn-success mg-r-5" type="submit" name="simpan">Simpan</button>
            <a href="?hal=dtanggota" class="btn btn-secondary">Batal</a>
          </div><!-- form-layout-footer -->
        </div><!-- col-8 -->
      </div>
    </form>
  </div><!-- card-body -->
</div><!-- card -->

<?php
  if (isset($_POST['simpan'])) 
  {
    $mail = $_POST['ml'];
    $pass = $_POST['ps'];
    $foto = $_FILES['ft']['name'];

    $nik = $_POST['nik'];
    $nma = $_POST['nmlngkp'];
    $alm = $_POST['alm'];
    $tmp = $_POST['tmp_lhr'];
    $tgl = $_POST['tgl_lhr'];
    $nhp = $_POST['notel'];
    $krj = $_POST['pkrj'];
    $pdd = $_POST['didik'];
    $tms = $_POST['tmk'];
    $nms = $_POST['nm_si'];
    $mdk = $_POST['mdk'];
    $sld = $_POST['sld'];
    $sts = $_POST['st'];

    $que1 = mysqli_query($kon, "SELECT * FROM `tbl_anggota` WHERE `nik` = '$nik'");
    if (mysqli_num_rows($que1) == 1) 
    {
    ?>
        <div class="alert alert-warning" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Maaf!</strong> Username Telah Digunakan.
        </div><!-- alert -->
    <?php
    }
    else
    {
      $ext1   = array('png','jpg','jpeg','gif');
      $x = explode('.', $foto);
      $ext2 = strtolower(end($x));
      $ukuran = $_FILES['ft']['size'];
      $file_tmp = $_FILES['ft']['tmp_name'];  

      if(in_array($ext2, $ext1) === true)
      {
          if($ukuran < 5242880)
          {           
              move_uploaded_file($file_tmp, 'img/'.$foto);
              $que2 = mysqli_query($kon, "INSERT INTO `tbl_users` (`role_id`, `nama`, `email`, `password`, `foto`) VALUES ('2', '$nma', '$mail', md5('$pass'), '$foto')");
              if ($que2) 
              {
                $que3 = mysqli_query($kon, "SELECT * FROM `tbl_users` ORDER BY `id` DESC LIMIT 1");
                $dta3 = mysqli_fetch_array($que3);
                $idna = $dta3['id'];
                $que4 = mysqli_query($kon, "INSERT INTO `tbl_anggota` (`id_users`, `nik`, `nama_lengkap`, `alamat`, `tmp_lhr`, `tgl_lhr`, `tlp`, `pekerjaan`, `pendidikan`, `mulai_kerja`, `suami_istri`, `tgl_daftar`, `saldo_awal`, `saldo_akhir`, `status_anggota`) VALUES ('$idna', '$nik', '$nma', '$alm', '$tmp', '$tgl', '$nhp', '$krj', '$pdd', '$tms', '$nms', '$mdk', '$sld', '$sld', '$sts')");
                if ($que4) 
                {
                  
                  echo "<script>window.location = '?hal=dtanggota&error=0';</script>";
                }
                else
                {
                  //echo "<script>window.location = '?hal=dtanggota&error=1';</script>";
                  echo '> '.$idna.'</br>';
                  echo '> '.$nik.'</br>';
                  echo '> '.$nma.'</br>';
                  echo '> '.$alm.'</br>';
                  echo '> '.$tmp.'</br>';
                  echo '> '.$tgl.'</br>';
                  echo '> '.$nhp.'</br>';
                  echo '> '.$krj.'</br>';
                  echo '> '.$pdd.'</br>';
                  echo '> '.$tms.'</br>';
                  echo '> '.$nms.'</br>';
                  echo '> '.$mdk.'</br>';
                  echo '> '.$sld.'</br>';
                  echo '> '.$sts.'</br>';
                }
              }
              else
              {
              ?>
                  <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <strong class="d-block d-sm-inline-block-force">Terjadi Kesalahan!</strong> Gagal Proses Data Mohon ulangi.
                  </div><!-- alert -->
              <?php
              }
          }
          else
          {
              echo 'UKURAN FILE TERLALU BESAR';
          }
      }
      else
      {
          echo 'EXTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
      }
    }        
  }
?>