<?php
include 'include/db.php';?>
<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tambah Pinjam</div>
  <div class="card-body pd-sm-30 form-layout form-layout-5">
    <form action="" method="post">
    <?php
    if ($_SESSION['level']==1) 
    {
      ?>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Nama Anggota:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih" tabindex="-1" aria-hidden="true" name="nm_angg">
          <option label="Pilih"></option>
          <?php
          $que1 = mysqli_query($kon, "SELECT * FROM `tbl_anggota`");
          while ($dta = mysqli_fetch_array($que1)) 
          {
            echo '<option value="'.$dta['id'].'">';
            echo $dta['nama_lengkap'].'</option>';
          }?>
        </select>
      </div>
    </div><!-- row -->
      <?php
    }
    else
    {
      $ida = $_SESSION['ida'];
      $que2 = mysqli_query($kon, "SELECT * FROM `tbl_anggota` WHERE `id` = $ida");
      $dta2 = mysqli_fetch_array($que2);
      ?>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Nama Anggota:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="text" class="form-control" name="nm_angg" value="<?php echo $dta2['nama_lengkap'];?>">
      </div>
    </div>
      <?php
    }?>

    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal Pinjam:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="date" class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" id="dp1556170438293" name="tgl_pjm">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Nominal Pinjam:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="text" class="form-control" placeholder="Masukan Jumlah Nominal Pinjam" name="nom_pjm">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Bunga Pinjaman:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="text" class="form-control" placeholder="Masukan Jumlah Bunga Pinjaman" name="bung_pjm">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tenor:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih" tabindex="-1" aria-hidden="true" name="jang_ang">
          <option label="Pilih"></option>
          <?php
          $que3 = mysqli_query($kon, "SELECT * FROM `tbl_tenor` WHERE `status` = 1");
          while ($dta3 = mysqli_fetch_assoc($que3)) 
          {
            echo '<option value="'.$dta3['id'].'"';

            echo '>'.$dta3['tenor'].'</option>';
          }?>
        </select>
      </div>
    </div><!-- row -->
    <?php
    if ($_SESSION['level']==1) 
    {
      ?>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Status:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih" tabindex="-1" aria-hidden="true" name="status">
          <option label="Pilih"></option>
          <option value="1">Disetujui</option>
          <option value="2">Tidak Disetujui</option>
        </select>
      </div>
    </div><!-- row -->
      <?php
    }
    ?>
    <div class="row row-xs mg-t-30">
      <div class="col-sm-8 mg-l-auto">
        <div class="form-layout-footer">
          <button class="btn btn-success mg-r-5" name="simpan">Simpan</button>
          <a href="?hal=dtpinjam" class="btn btn-secondary">Batal</a>
        </div><!-- form-layout-footer -->
      </div><!-- col-8 -->
    </div>
  </form>
  </div><!-- card-body -->
</div><!-- card -->

<?php
      if (isset($_POST['simpan'])) 
      {
        $nm = $_POST['nm_angg'];
        $kt = $_POST['tgl_pjm'];
        $np = $_POST['nom_pjm'];
        $bp = $_POST['bung_pjm'];
        $ja = $_POST['jang_ang'];
        if ($_SESSION['level']==1) 
        {
          $st = $_POST['status'];
        }
        else
        {
          $st = '0';
        }

        $qadd = mysqli_query($kon, "INSERT INTO `tbl_pinjam` (`id`, `id_anggota`, `tgl_pinjam`, `nominal_pinjam`, `bunga_pinjam`, `tenor_angsuran`, `status`) VALUES (NULL, '$nm', '$kt', '$np', '$bp', '$ja', '$st') ");
        if ($qadd) 
        {
          
          echo "<script>window.location = '?hal=dtpinjam&error=0';</script>";
        }
        else
        {
          echo "<script>window.location = '?hal=dtpinjam&error=1';</script>";
        }
      }
    ?>