<?php
include 'include/db.php';
if ($_SESSION['level']!=1) 
{
    echo "<script>window.location = '?hal=main';</script>";
}
?>

<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tambah Angsuran</div>
  <div class="card-body pd-sm-30 form-layout form-layout-5">
    <form action="" method="post">
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label" name="pin"><span class="tx-danger">*</span> Pinjaman:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih" tabindex="-1" aria-hidden="true">
            <?php
              $sql = "SELECT * FROM `tbl_anggota`";
              $que = mysqli_query($kon, $sql);
              while ($dta = mysqli_fetch_assoc($que)) 
              {
                  echo '<option value="'.$dta['nik'].'">'.$dta['nama_lengkap'].`</optiion>`;
              }
             ?>
          </select>
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Bayar:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" name="by" placeholder="Masukan Jumlah Bayar">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Kode Angsuran:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" name="kd" placeholder="Masukan Kode Angsuran">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Nominal Angsuran:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" name="na" placeholder="Masukan Nominal Angsuran">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal Angsuran:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="date" class="form-control fc-datepicker hasDatepicker" name="tgla" placeholder="MM/DD/YYYY" id="dp1556170438293">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal jatuh Tempo:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="date" class="form-control fc-datepicker hasDatepicker" name="tglj" placeholder="MM/DD/YYYY" id="dp1556170438293">
        </div>
      </div>
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label" name="st"><span class="tx-danger">*</span> Status Angsuran:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <label class="rdiobox">
            <input name="rdio" type="radio">
            <span>Pending</span>
          </label>
          <label class="rdiobox">
            <input name="rdio" type="radio">
            <span>Aktif</span>
          </label>
          <label class="rdiobox">
            <input name="rdio" type="radio">
            <span>Nonaktif</span>
          </label>
        </div>
      </div>
      <div class="row row-xs mg-t-30">
        <div class="col-sm-8 mg-l-auto">
          <div class="form-layout-footer">
            <button class="btn btn-success mg-r-5" name="simpan">Simpan</button>
            <a href="?hal=dtangsuran"><button class="btn btn-secondary">Batal</button></a>
          </div><!-- form-layout-footer -->
        </div><!-- col-8 -->
      </div>
    </div><!-- card-body -->
  </div><!-- card -->

  <?php
        if (isset($_POST['simpan'])) 
        {
            $pinj = $_POST['pin'];
            $bya = $_POST['by'];
            $kda = $_POST['kd'];
            $noa = $_POST['na'];
            $tglan = $_POST['tgla'];
            $tglp = $_POST['tglj'];
            $stts = $_POST['st'];

            $que1 = mysqli_query($kon, "INSERT INTO `tbl_angsuran` (`pinjaman_id`, `bayar_id`, `kode_angsuran`, `nominal_angsuran`, `tgl_angsuran`, `jatuh_tempo`, `status_angsuran`) VALUES ('$pinj', '$bya', '$kda', '$noa', '$tglan', '$tglp', '$stts')");
            if ($que1) 
            {
                echo "<script>window.location = '?hal=dtangsuran&error=0';</script>";
            }
            else
            {
                echo "<script>window.location = '?hal=dtangsuran&error=1';</script>";
            }
        }
        ?>