<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tabel Data Simpan</div>
  <div class="card-body pd-sm-30">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <a href="?hal=addsimpan" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus mg-r-10"></i> Tambah Data</a>
      </div><!-- col-sm -->
      <div class="col-sm-6 col-md-3 mg-t-20 mg-sm-t-0">
        <a href="cetak/pr_users.php" class="btn btn-success btn-block mg-b-10"><i class="fa fa-print mg-r-10"></i> Cetak Data</a>
      </div><!-- col-sm -->
    </div><!-- row -->
    <hr>
    <div class="table-wrapper">
      <table id="datatable1" class="table display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-5p">No</th>
            <th class="wd-20p">Nama Anggota</th>
            <th class="wd-15p">Tanggal Simpan</th>
            <th class="wd-10p">Nominal</th>
            <td class="wd-20p">Metode Pembayaran</td>
            <th class="wd-20p">status</th>
            <th class="wd-20p">Aksi</th>
          </tr>
        </thead>
        <tbody>
            <?php
            include "include/db.php";
            $i = 1;
            if ($_SESSION['level'] == 1) 
            {
              $query = mysqli_query($kon, "SELECT * FROM `tbl_simpan` ORDER BY `id` DESC");
            }
            else
            {
              $ida = $_SESSION['ida'];
              $query = mysqli_query($kon, "SELECT * FROM `tbl_simpan` WHERE `id_anggota` = $ida ORDER BY `id` DESC");
            }
            
            while ($data = mysqli_fetch_assoc($query)) 
            {
              $id = $data['id'];
              $idb = $data['bayar_id'];
            ?>

          <tr>  
            <td><?php echo $i++;?></td>
            <td><?php 
              $ida = $data['id_anggota'];
              $que1 = mysqli_query($kon, "SELECT * FROM `tbl_anggota` WHERE `id` = $ida");
              $dta1 = mysqli_fetch_array($que1);
              echo $dta1['nama_lengkap'];
            ?></td>
            <td><?php echo tgl_indo($data['tgl_simpan']);?></td>
            <td><?php echo rupiah($data['nominal_simpan']);?></td>
            <td><?php
              if ($data['metode']==1) 
              {
                echo "Oleh CS";
              }
              else
              {
                echo "Transfer";
              }
            ?></td>
            <td><?php
              if ($data['status']==1) 
              {
                echo '<span class="label label-success"><font color="green">Terferivikasi</font></span>';
              }
              elseif ($data['status']==2) 
              {
                echo '<span class="label label-danger"><font color="red">Tidak diferivikasi</font></span>';
              }
              else
              {
                echo '<span class="label label-warning"><font color="orange">Belum Terferivikasi</font></span>';
              }
            ?></td>
            <td>
              <?php
              if ($idb != NULL) 
              {
              ?>

              <a href="#" data-toggle="modal" data-target="#detail<?php echo $id;?>"><i class="icon ion-search"></i></a>  &nbsp;
              <?php
              }?>
              <!--
              <a href="?hal=updanggota"><i class="icon ion-edit"></i></a>  &nbsp;
              <a href="?hal=delanggota" onclick="return confirm('Anda Yakin Menghapus?');"><i class="icon ion-close"></i></a>
              -->
              <?php
              if ($idb != NULL) 
              {
              ?>

              <!-- LARGE MODAL -->
              <div id="detail<?php echo $id;?>" class="modal fade">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                      <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Detail Pembayaran</h6>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body pd-20">
                      <?php
                      $que2 = mysqli_query($kon, "SELECT * FROM `tbl_bayar` WHERE `id` = $idb");
                      $dta2 = mysqli_fetch_array($que2);
                      ?>

                      <table width="100%" class="table table-hover table-bordered" border="1">
                        <tr>
                          <td><b>No Rek</b></td>
                          <td>:</td>
                          <td><?php echo $dta2['no_rek'];?></td>
                          <td>Bukti Transaksi</td>
                        </tr>
                        <tr>
                          <td><b>Nominal Transfer</b></td>
                          <td>:</td>
                          <td><?php echo rupiah($dta2['nominal_trf']);?></td>
                          <td rowspan="7"><a href="img/<?php echo $dta2['foto'];?>" target="_blank"><img src="img/<?php echo $dta2['foto'];?>" width="200px"></a></td>
                        </tr>
                        <tr>
                          <td><b>Tanggal Bayar</b></td>
                          <td>:</td>
                          <td><?php echo tgl_indo($dta2['tgl_bayar']);?></td>
                        </tr>
                        <tr>
                          <td><b>Tanggal Transfer</b></td>
                          <td>:</td>
                          <td><?php echo tgl_indo($dta2['tgl_transfer']);?></td>
                        </tr>
                        <tr>
                          <td><b>Nama Rekening</b></td>
                          <td>:</td>
                          <td><?php echo $dta2['nama_rek'];?></td>
                        </tr>
                        <tr>
                          <td><b>Nama Bank</b></td>
                          <td>:</td>
                          <td><?php echo $dta2['nama_bank'];?></td>
                        </tr>
                        <tr>
                          <td><b>Cabang</b></td>
                          <td>:</td>
                          <td><?php echo $dta2['cabang_bank'];?></td>
                        </tr>
                        <tr>
                          <td><b>Keterangan</b></td>
                          <td>:</td>
                          <td><?php echo $dta2['ket'];?></td>
                        </tr>
                      </table>
                      
                    </div><!-- modal-body -->
                    <div class="modal-footer">
                      <?php
                      if ($_SESSION['level']==1) 
                      {
                        echo '<a href="?hal=accepted&id='.$id.'" class="btn btn-primary pd-x-20">Verifikasi Pembayaran</a>';
                        echo '&nbsp;';
                        echo '<a href="?hal=canceled&id='.$id.'" class="btn btn-danger pd-x-20">Tolak Verifikasi</a>';
                      }?>

                      <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Keluar</button>
                    </div>
                  </div>
                </div><!-- modal-dialog -->
              </div><!-- modal -->
              <?php
              }?>
              
            </td>
          </tr>
          <?php
           }
          ?>

        </tbody>
      </table>
    </div><!-- table-wrapper -->
  </div><!-- card-body -->
</div><!-- card -->