<?php include 'include/db.php';
if ($_SESSION['level'] == 1) 
{

?>
<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tambah Data Simpan</div>
  <div class="card-body pd-sm-30 form-layout form-layout-5">
    <form action="" method="post" enctype="multipart/form-data">
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Nama Anggota:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <select name="na" class="form-control select2 select2-show-search" data-placeholder="Pilih Nama Angota" tabindex="-1" aria-hidden="true">
          <?php
          $sql = "SELECT * FROM `tbl_anggota`";
          $que = mysqli_query($kon, $sql);
          while ($dta = mysqli_fetch_assoc($que)) 
          {
            echo '<option value="'.$dta['id'].'">'.$dta['nama_lengkap'].`</optiion>`;
          }
          ?>
          </select>
        </div>
      </div>

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Nominal Simpan:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" name="ns" class="form-control" placeholder="Masukan Jumlah Simpanan">
        </div>
      </div>  

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal Simpan:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="date" name="tg" class="form-control">
        </div>
      </div>  

      <div class="row row-xs mg-t-30">
        <div class="col-sm-8 mg-l-auto">
          <div class="form-layout-footer">
            <button class="btn btn-success mg-r-5" name="simpan">Simpan</button>
            <a href="?hal=dtsimpan"><button class="btn btn-secondary">Batal</button></a>
          </div><!-- form-layout-footer -->
        </div><!-- col-8 -->
      </div>  
    </form>   
  </div><!-- card-body -->
</div><!-- card -->

<?php
  if (isset($_POST['simpan'])) 
  {
    $nma = $_POST['na'];
    $nos = $_POST['ns'];
    $tgl = $_POST['tg'];

    $que1 = mysqli_query($kon, "INSERT INTO `tbl_simpan` (`id`, `id_anggota`, `bayar_id`, `tgl_simpan`, `nominal_simpan`, `metode`, `status`) VALUES (NULL, '$nma', NULL, '$tgl', '$nos', '1', '1')");
    if ($que1) 
    {
      $que2 = mysqli_query($kon, "SELECT * FROM `tbl_anggota` WHERE `id` = $nma");
      $dta2 = mysqli_fetch_array($que2);
      $sld1 = $dta2['saldo_akhir'];
      $sld2 = $sld1 + $nos;
      $que5 = mysqli_query($kon, "UPDATE `tbl_anggota` SET `saldo_akhir` = '$sld2' WHERE `tbl_anggota`.`id` = $nma");
      if ($que5) 
      {
        echo "<script>window.location = '?hal=dtsimpan&error=0';</script>";
      }
      else
      {
        echo "<script>window.location = '?hal=dtsimpan&error=2';</script>";
      }
      
    }
    else
    {
      echo "<script>window.location = '?hal=dtsimpan&error=1';</script>";
    }
  }

}
elseif ($_SESSION['level']==2) 
{
  $ida = $_SESSION['ida'];
  $que2 = mysqli_query($kon, "SELECT * FROM `tbl_anggota` WHERE `id` = $ida");
  $dta2 = mysqli_fetch_array($que2);
  $sld1 = $dta2['saldo_akhir'];
?>
<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tambah Data Simpan</div>
  <div class="card-body pd-sm-30 form-layout form-layout-5">
    <form action="" method="post" enctype="multipart/form-data">
      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Nama Anggota:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" name="nm" value="<?php echo $dta2['nama_lengkap'];?>" class="form-control" disabled>
        </div>
      </div>

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Nominal Simpan:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" name="ns" class="form-control" placeholder="Masukan Jumlah Simpanan">
        </div>
      </div>  

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>No Rekening:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" name="nr" class="form-control" placeholder="Masukan Nomor Rekening">
        </div>
      </div>  

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal Transfer:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="date" name="tg" class="form-control">
        </div>
      </div>  

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Nama Rekening:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" name="na" class="form-control" placeholder="Masukan Nama Rekening">
        </div>
      </div> 

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Nama Bank:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" name="nb" class="form-control" placeholder="Masukan Nama Bank">
        </div>
      </div> 

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Cabang:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" name="nc" class="form-control" placeholder="Masukan Cabang Bank">
        </div>
      </div> 

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Keterangan:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <textarea rows="3" class="form-control" placeholder="Masukan Keterangan Jika Ada" name="kt"></textarea>
        </div>
      </div>

      <div class="row row-xs mg-t-20">
        <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Foto Bukti Nota:</label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <label class="custom-file">
            <input type="file" id="file2" name="ft" class="custom-file-input">
            <span class="custom-file-control custom-file-control-primary"></span>
          </label>
        </div>
      </div>
      <div class="row row-xs mg-t-30">
        <div class="col-sm-8 mg-l-auto">
          <div class="form-layout-footer">
            <button class="btn btn-success mg-r-5" name="addbayar">Simpan</button>
            <a href="?hal=dtsimpan"><button class="btn btn-secondary">Batal</button></a>
          </div><!-- form-layout-footer -->
        </div><!-- col-8 -->
      </div>  
    </form>   
  </div><!-- card-body -->
</div><!-- card -->
<?php
  if (isset($_POST['addbayar'])) 
  {
    $nm = $_POST['nm'];
    $ns = $_POST['ns'];
    $nr = $_POST['nr'];
    $tg = $_POST['tg'];
    $na = $_POST['na'];
    $nb = $_POST['nb'];
    $nc = $_POST['nc'];
    $kt = $_POST['kt'];
    $ft = $_FILES['ft'];
    $tgl = date('Y-m-d');
    $ext1   = array('png','jpg','jpeg','gif');
    $ft = $_FILES['ft']['name'];
    $x = explode('.', $ft);
    $ext2 = strtolower(end($x));
    $ukuran = $_FILES['ft']['size'];
    $file_tmp = $_FILES['ft']['tmp_name'];  

    if(in_array($ext2, $ext1) === true)
    {
        if($ukuran < 5242880)
        {           
            move_uploaded_file($file_tmp, 'img/'.$ft);
            $que2 = mysqli_query($kon, "INSERT INTO `tbl_bayar` (`id`, `id_anggota`, `no_rek`, `nominal_trf`, `tgl_bayar`, `tgl_transfer`, `nama_rek`, `nama_bank`, `cabang_bank`, `ket`, `foto`) VALUES (NULL, '$ida', '$nr', '$ns', '$tgl', '$tg', '$na', '$nb', '$nc', '$kt', '$ft')");
            if ($que2) 
            {

              $que3 = mysqli_query($kon, "SELECT * FROM `tbl_bayar` WHERE `id_anggota` = $ida ORDER BY `id` DESC");
              $dta3 = mysqli_fetch_array($que3);
              $idb = $dta3['id'];
              $que4 = mysqli_query($kon, "INSERT INTO `tbl_simpan` (`id`, `id_anggota`, `bayar_id`, `tgl_simpan`, `nominal_simpan`, `metode`, `status`) VALUES (NULL, '$ida', '$idb', '$tgl', '$ns', '2', '0')");
              if ($que4) 
              {
                
                echo "<script>window.location = '?hal=dtsimpan&error=0';</script>";
              }
              else
              {
                echo "<script>window.location = '?hal=dtsimpan&error=1';</script>";
              }
            }
            else
            {
            ?>
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Terjadi Kesalahan!</strong> Gagal Proses Data Mohon ulangi.
                </div><!-- alert -->
            <?php
            }
        }
        else
        {
            echo 'UKURAN FILE TERLALU BESAR';
        }
    }
    else
    {
        echo 'EXTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
    }
  }
}
else
{
  echo "Tidak Ada";
}
?>