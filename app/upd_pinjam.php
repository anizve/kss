<?php
include 'include/db.php';
$id = $_GET['id'];
$que = mysqli_query($kon, "SELECT * FROM `tbl_pinjam` WHERE `id` = $id");
$dta = mysqli_fetch_array($que);

?>
<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Edit Pinjam</div>
  <div class="card-body pd-sm-30 form-layout form-layout-5">
    <form action="" method="post">
    <input type="hidden" name="idn" value="<?php echo $id;?>">
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Nama Anggota:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <?php
        $ida2 = $dta['id_anggota'];
        $que2 = mysqli_query($kon, "SELECT * FROM `tbl_anggota` WHERE `id` = $ida2");
        $dta2 = mysqli_fetch_array($que2);
        ?>
        <input type="text" class="form-control" name="nm_angg" value="<?php echo $dta2['nama_lengkap'];?>" disabled>
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tanggal Pinjam:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="date" class="form-control" name="tgl_pjm" value="<?php echo $dta['tgl_pinjam'];?>">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span>Nominal Pinjam:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="text" class="form-control" name="nom_pjm" value="<?php echo $dta['nominal_pinjam'];?>">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Bunga Pinjaman:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <input type="text" class="form-control" name="bung_pjm" value="<?php echo $dta['bunga_pinjam'];?>">
      </div>
    </div>
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Tenor:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih" tabindex="-1" aria-hidden="true" name="jang_ang">
          <option label="Pilih"></option>
          <?php
          $que3 = mysqli_query($kon, "SELECT * FROM `tbl_tenor` WHERE `status` = 1");
          while ($dta3 = mysqli_fetch_assoc($que3)) 
          {
            echo '<option value="'.$dta3['id'].'"';
            if ($dta['tenor_angsuran'] == $dta3['id']) 
            {
              echo ' selected=""';
            }
            echo '>'.$dta3['tenor'].'</option>';
          }?>

        </select>
      </div>
    </div><!-- row -->
    <div class="row row-xs mg-t-20">
      <label class="col-sm-2 form-control-label"><span class="tx-danger">*</span> Status:</label>
      <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih" tabindex="-1" aria-hidden="true" name="status">
          <option label="Pilih"></option>
          <option value="0"<?php if($dta['status']==0){echo ' selected=""';}?>>Belum Proses</option>
          <option value="1"<?php if($dta['status']==1){echo ' selected=""';}?>>Disetujui</option>
          <option value="2"<?php if($dta['status']==2){echo ' selected=""';}?>>Tidak Disetujui</option>
        </select>
      </div>
    </div><!-- row -->
    <div class="row row-xs mg-t-30">
      <div class="col-sm-8 mg-l-auto">
        <div class="form-layout-footer">
          <button class="btn btn-success mg-r-5" name="updsave">Simpan</button> &nbsp;
          <a href="?hal=delpinjam&id=<?php echo $id;?>" class="btn btn-danger" onclick="return confirm('Anda Yakin Menghapus?');">Hapus Data</a> &nbsp;
          <a href="?hal=dtpinjam" class="btn btn-secondary">Batal</a>
        </div><!-- form-layout-footer -->
      </div><!-- col-8 -->
    </div>
  </form>
  </div><!-- card-body -->
</div><!-- card -->

<?php
  if (isset($_POST['updsave'])) 
  {
    $idn = $_POST['idn'];
    $nma = $dta['id_anggota'];
    $tgl = $_POST['tgl_pjm'];
    $npj = $_POST['nom_pjm'];
    $bpj = $_POST['bung_pjm'];
    $tnr = $_POST['jang_ang'];
    $sts = $_POST['status'];

    $qupd = mysqli_query($kon, "UPDATE `tbl_pinjam` SET 
      `tgl_pinjam` = '$tgl', 
      `nominal_pinjam` = '$npj', 
      `bunga_pinjam` = '$bpj', 
      `tenor_angsuran` = '$tnr', 
      `status` = '$sts' 
      WHERE `tbl_pinjam`.`id` = $idn");
    if ($qupd) 
    {
      
      echo "<script>window.location = '?hal=dtpinjam&error=0';</script>";
    }
    else
    {
      echo "<script>window.location = '?hal=dtpinjam&error=1';</script>";
    }
  }
?>