<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tabel Data Semua Pengguna</div>
  <div class="card-body pd-sm-30">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <a href="?hal=addpengguna" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus mg-r-10"></i> Tambah Data</a>
      </div><!-- col-sm -->
      <div class="col-sm-6 col-md-3 mg-t-20 mg-sm-t-0">
        <a href="cetak/pr_users.php" class="btn btn-success btn-block mg-b-10"><i class="fa fa-print mg-r-10"></i> Cetak Data</a>
      </div><!-- col-sm -->
    </div><!-- row -->
    <hr>
    <div class="table-wrapper">
      <table id="datatable1" class="table display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-5p">No</th>
            <th class="wd-10p">Foto</th>
            <th class="wd-15p">Nama Lengkap</th>
            <th class="wd-20p">Email</th>
            <th class="wd-10p">Hak Akses</th>            
            <th class="wd-25p">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <?php
            include "include/db.php";
            $i = 1;
            $query = mysqli_query($kon, "SELECT * FROM `tbl_users` ORDER BY `id` DESC");
            while ($data = mysqli_fetch_assoc($query)) 
            {
              $id = $data['id'];
            ?>
          <tr>
            <td><?php echo $i++;?></td>
            <td><img src="img/<?php echo $data['foto'];?>" width="50px"></td>
            <td><?php echo $data['nama'];?></td>
            <td><?php echo $data['email'];?></td>
            <td><?php
              $lv =  $data['role_id'];
              $que1 = mysqli_query($kon, "SELECT * FROM `tbl_role` WHERE `id` = $lv");
              $dta1 = mysqli_fetch_array($que1);
              echo $dta1['rolename'];
            ?></td> 
            
            <td>
              <a href="?hal=updpengguna&id=<?php echo $id;?>"><i class="icon ion-edit"></i></a>  &nbsp;
              <a href="?hal=delpengguna&id=<?php echo $id;?>" onclick="return confirm('Anda Yakin Menghapus?');"><i class="icon ion-close"></i></a>
            </td>
          </tr>
           <?php
           }
          ?>
        </tbody>
      </table>
    </div><!-- table-wrapper -->
  </div><!-- card-body -->
</div><!-- card -->