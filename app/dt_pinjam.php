<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tabel Data Pinjam</div>
  <div class="card-body pd-sm-30">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <a href="?hal=addpinjam" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus mg-r-10"></i> Tambah Data</a>
      </div><!-- col-sm -->
      <div class="col-sm-6 col-md-3 mg-t-20 mg-sm-t-0">
        <a href="cetak/pr_users.php" class="btn btn-success btn-block mg-b-10"><i class="fa fa-print mg-r-10"></i> Cetak Data</a>
      </div><!-- col-sm -->
    </div><!-- row -->
    <hr>
    <div class="table-wrapper">
      <table id="datatable1" class="table display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-5p">No</th>
            <th class="wd-15p">Nama Anggota</th>
            <th class="wd-20p">Tanggal Pinjam</th>
            <th class="wd-15p">Nominal</th>
            <th class="wd-10p">Bunga</th>
            <th class="wd-10p">Tenor</th>
            <th class="wd-10p">Status</th>
            <th class="wd-20p">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <?php
            include "include/db.php";
            $i = 1;
            if ($_SESSION['level'] == 1) 
            {
              $query = mysqli_query($kon, "SELECT * FROM `tbl_pinjam` ORDER BY `id` DESC");
            }
            else
            {
              $ida = $_SESSION['ida'];
              $query = mysqli_query($kon, "SELECT * FROM `tbl_pinjam` WHERE `id_anggota` = $ida ORDER BY `id` DESC");
            }
            
            while ($data = mysqli_fetch_assoc($query)) 
            {
              $id = $data['id'];
            ?>
            <td><?php echo $i++;?></td>
            <td><?php 
              $ida = $data['id_anggota'];
              $que1 = mysqli_query($kon, "SELECT * FROM `tbl_anggota` WHERE `id` = $ida");
              $dta1 = mysqli_fetch_array($que1);
              echo $dta1['nama_lengkap'];
            ?></td>
            <td><?php echo tgl_indo($data['tgl_pinjam']);?></td>
            <td><?php echo rupiah($data['nominal_pinjam']);?></td>
            <td><?php echo $data['bunga_pinjam'];?>%</td>
            <td><?php 
              $tn = $data['tenor_angsuran'];
              $que2 = mysqli_query($kon, "SELECT * FROM `tbl_tenor` WHERE `id` = $tn");
              $dta2 = mysqli_fetch_assoc($que2); 
              echo $dta2['tenor'];
            ?>
            </td>
            <td><?php
              if ($data['status']==0) 
              {
                echo '<font color="#ffbf00">Dalam Proses</font>';
              }
              elseif ($data['status']==1) 
              {
                echo '<font color="#008000">Disetuji</font>';
              }
              else
              {
                echo '<font color="#ff0000">Tidak Disetuji</font>';
              }
            ?></td>
            <td>
              <a href="?hal=angsuranagt&idp=<?php echo $id;?>&ida=<?php echo $ida;?>"><i class="icon ion-search"></i></a>  &nbsp;
              <?php
              if ($_SESSION['level']==1) 
              {
                ?>
              <a href="?hal=stpinjam&id=<?php echo $id;?>&st=1"><i class="icon ion-checkmark"></i></a> &nbsp;
              <a href="?hal=stpinjam&id=<?php echo $id;?>&st=2"><i class="icon ion-close"></i></a> &nbsp;
              <a href="?hal=updpinjam&id=<?php echo $id;?>"><i class="icon ion-edit"></i></a>
                <?php
              }?>
              
            </td>
          </tr>
          <?php
           }
          ?>
        </tbody>
      </table>
    </div><!-- table-wrapper -->
  </div><!-- card-body -->
</div><!-- card -->

<?php
if (isset($_GET['delid'])) 
{
    $delid = $_GET['id'];

    $que5 = mysqli_query($kon, "DELETE FROM `tbl_pinjam` WHERE `id` = $id");
    if ($que5) 
    {
        echo "<script>window.location = '?hal=dtbarang&error=0';</script>";
    }
    else
    {
        echo "<script>window.location = '?hal=dtbarang&error=1';</script>";
    }
}
?>