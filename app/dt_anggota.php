
<div class="card bd-primary mg-t-20">
  <div class="card-header bg-primary tx-white">Tabel Data Semua Anggota</div>
  <div class="card-body pd-sm-30">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <a href="?hal=addanggota" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus mg-r-10"></i> Tambah Data</a>
      </div><!-- col-sm -->
      <div class="col-sm-6 col-md-3 mg-t-20 mg-sm-t-0">
        <a href="cetak/pr_users.php" class="btn btn-success btn-block mg-b-10"><i class="fa fa-print mg-r-10"></i> Cetak Data</a>
      </div><!-- col-sm -->
    </div><!-- row -->
    <hr>
    <div class="table-wrapper">
      <table id="datatable1" class="table display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-5p">No</th>
            <th class="wd-10p">NIK</th>
            <th class="wd-20p">Nama Lengkap</th>
            <th class="wd-15p">Nama Suami / Istri</th>
            <th class="wd-10p">Tanggal Daftar</th>
            <th class="wd-10p">Saldo</th>
            <th class="wd-10p">Status</th>
            <th class="wd-25p">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <?php
            include "include/db.php";
            $i = 1;
            $query = mysqli_query($kon, "SELECT * FROM `tbl_anggota` ORDER BY `id` DESC");
            while ($data = mysqli_fetch_assoc($query)) 
            {
              $id = $data['id'];
            ?>
            <td><?php echo $i++;?></td>
            <td><?php echo $data['nik'];?></td>
            <td><?php echo $data['nama_lengkap'];?></td>
            <td><?php echo $data['suami_istri'];?></td>
            <td><?php echo tgl_indo($data['tgl_daftar']);?></td>
            <td><?php echo rupiah($data['saldo_akhir']);?></td>
            <td><?php 
              if ($data['status_anggota'] == 0) 
              {
                echo "pending";
              }
              elseif ($data['status_anggota'] == 1)
              {
                echo "aktif";
              }
              else
              {
                echo "nonaktif";
              }
            ?>
            </td>
            <td>
              <a href="#" data-toggle="modal" data-target="#detail<?php echo $id;?>"><i class="icon ion-search"></i></a>  &nbsp;
              <a href="?hal=updanggota&id=<?php echo $id;?>"><i class="icon ion-edit"></i></a>  &nbsp;
              <a href="?hal=dtanggota&hapus=<?php echo $id;?>" onclick="return confirm('Anda Yakin Menghapus?');"><i class="icon ion-close"></i></a>
            </td>
          </tr>
          <!-- LARGE MODAL -->
          <div id="detail<?php echo $id;?>" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Detail Anggota</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-20">
                  <div class="row">
                    <div class="col-sm-6">
                      <font color="#FFFFFFF" class="mg-b-5">
                        ---------------------------------------------------------------------------
                      </font>
                    </div>
                    <div class="col-sm-6">
                      <font color="#FFFFFFF" class="mg-b-5">
                        ---------------------------------------------------------------------------
                      </font>
                    </div>
                  </div>
                  
                  <div class="card-body pd-sm-30">
                    <dl class="row">
                      <dt class="col-sm-6 tx-inverse">No Induk Kependudukan</dt>
                      <dd class="col-sm-6"><?php echo $data['nik'];?></dd>
                    
                      <dt class="col-sm-6 tx-inverse">Nama Lengkap</dt>
                      <dd class="col-sm-6"><?php echo $data['nama_lengkap'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Alamat</dt>
                      <dd class="col-sm-6"><?php echo $data['alamat'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Tempat Lahir</dt>
                      <dd class="col-sm-6"><?php echo $data['tmp_lhr'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Tanggal Lahir</dt>
                      <dd class="col-sm-6"><?php echo $data['tgl_lhr'];?></dd>

                      <dt class="col-sm-6 tx-inverse">No Telpon</dt>
                      <dd class="col-sm-6"><?php echo $data['tlp'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Pekerjaan</dt>
                      <dd class="col-sm-6"><?php echo $data['pekerjaan'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Pendidikan</dt>
                      <dd class="col-sm-6"><?php echo $data['pendidikan'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Mulai Kerja</dt>
                      <dd class="col-sm-6"><?php echo $data['mulai_kerja'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Suami/Istri</dt>
                      <dd class="col-sm-6"><?php echo $data['suami_istri'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Tanggal Daftar</dt>
                      <dd class="col-sm-6"><?php echo $data['tgl_daftar'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Saldo Awal</dt>
                      <dd class="col-sm-6"><?php echo $data['saldo_awal'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Saldo Akhir</dt>
                      <dd class="col-sm-6"><?php echo $data['saldo_akhir'];?></dd>

                      <dt class="col-sm-6 tx-inverse">Status</dt>
                      <dd class="col-sm-6">
                        <?php
                          if ($data['status_anggota'] == 0) 
                          {
                            echo "Pending";
                          }
                          elseif ($data['status_anggota'] == 1) 
                          {
                            echo "Aktif";
                          }
                          else
                          {
                            echo "Tidak Aktif";
                          }
                        ?>
                      </dd>
                    </dl>
                  </div>

                </div><!-- modal-body -->
                <div class="modal-footer">
                  <a href="?hal=detsaldo&id=<?php echo $id;?>" class="btn btn-info pd-x-20">Lihat Detail Transaksi</a>
                  <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Keluar</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

          <?php
           }
          ?>
        </tbody>
      </table>
    </div><!-- table-wrapper -->
  </div><!-- card-body -->
</div><!-- card -->

<?php 
if ($_GET['hal']=='hapus') 
{
  $id = $_GET['id'];
  $qdel = mysqli_query($kon, "DELETE FROM `tbl_anggota` WHERE `id` = $id");
  if ($qdel) 
  {
    echo "<script>window.location = '?hal=dtanggota&error=0';</script>";
  }
  else
  {
    echo "<script>window.location = '?hal=dtanggota&error=1';</script>";
  }
}
?>